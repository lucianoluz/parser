all:
	$(MAKE) -C src/

run:
	cd src/ && make run

clean:
	cd src/ && make clean
