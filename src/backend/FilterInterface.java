package backend;

/**
 * 
 * Interface used for creation of filters. Used by {@link TableData}.
 *
 * @author Douglas Vanny Bento
 *
 */

public interface FilterInterface
{
	/**
	 * 
	 * Tests if an object meets filter conditions
	 * 
	 * @param obj Object to be tested
	 * @return true if object meets filter conditions. false otherwise
	 * 
	 */
	public Boolean isOk(Object obj);
}
