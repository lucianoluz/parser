package backend;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.File;

import static java.lang.System.out;

/**
 * 
 * Climate files parser.
 *
 * @author Douglas Vanny Bento
 *
 */

public class GlobalParser implements ParserInterface
{
	public TableData parse(String content) throws ContentNotParseable
	{
		try
		{
			return (new ClimateParser()).parse(content);
		}
		catch(ContentNotParseable e)
		{
			try
			{
				return (new HidroParser()).parse(content);
			}
			catch(ContentNotParseable e2)
			{
				throw new ContentNotParseable();
			}
		}
	}
	
	public static void main(String[] args)
	{
		try
		{
			BufferedReader in = new BufferedReader(
				new InputStreamReader(
					new FileInputStream(
						new File("/media/Files/Dropbox_Ubuntu/Dropbox/UFPEL/Desenvolvimento de Software/parser/local/EH HS02_31102013_1650_COTA-086.txt")
					),
					"ISO-8859-1"
				)
			);
			
			String content = "";
			String line = null;
			
			while((line = in.readLine()) != null)
			{
				content = content + line + "\n";
			}
			
			TableData t = (new GlobalParser()).parse(content);
			
			out.println(t);
		}
		catch(Exception e)
		{
			out.println(e);
		}
	}
}
