package backend;

import java.util.Date;
import java.util.ArrayList;
import java.lang.Math;
import static java.lang.System.out;

/**
 * 
 * Simple {@link TableData} generator created for testind purposes.
 *
 * @author Douglas Vanny Bento
 *
 */

public class RandomGeneratorParser implements ParserInterface
{
	public TableData parse(String content) throws ContentNotParseable
	{
		TableData table = new TableData();
		
		table.setRawHeader("Cabecario teste\nNenhuma informacao aqui tem revelancia alguma\n\nEstacao = 213\nLatitude = 90N Longitude = 180W");
		
		table.setInfoField("station","213[LaLaLa]");
		table.setInfoField("lat","23.41N");
		table.setInfoField("lon","66.6W");
		
		try
		{
			table.addColumn("TimeStamp",Date.class);
			table.addColumn("Name",String.class);
			table.addColumn("Ammount",Integer.class);
			table.addColumn("Temperature",Float.class);
			table.addColumn("Wind",String.class);
		}
		catch(TableData.ColumnAlreadyExists e)
		{
		}
		
		int i;
		int limit = ((int)Math.random()*100) + 450;
		
		String allowed_characters = "abcdefghijklmnopqrstuvwxyz ";
		
		ArrayList<String> wind_directions = new ArrayList<String>();
		wind_directions.add("-");
		wind_directions.add("N");
		wind_directions.add("NW");
		wind_directions.add("W");
		wind_directions.add("SW");
		wind_directions.add("S");
		wind_directions.add("SE");
		wind_directions.add("E");
		wind_directions.add("NE");
		
		for(i=0; i<limit; i++)
		{
			ArrayList<Object> row = new ArrayList<Object>();
			
			row.add(new Date((long)(Math.random()*3.15569e10 + (43*3.15569e10))));
			
			String random_name = "";
			int random_name_length = ((int)(Math.random()*5)) + 5;
			int j=0;
			
			for(j=0; j<random_name_length; j++)
			{
				random_name = random_name + allowed_characters.charAt((int)(Math.random()*allowed_characters.length()));
			}
			
			row.add(random_name);
			row.add(new Integer((int)(Math.random()*100 + 1)));
			row.add(new Float((float)Math.random()*50 - 10));
			row.add(wind_directions.get((int)(Math.random()*wind_directions.size())));
			
			try
			{
				table.addRow(row);
			}
			catch(TableData.RowLengthMismatch e)
			{
				out.println("Error 2");
			}
			catch(TableData.TypeMismatch e)
			{
				out.println(e.received.toString());
				out.println(e.expected.toString());
			}
		}
		
		try
		{
			table.sort("TimeStamp");
		}
		catch(TableData.ColumnNotFound e)
		{
			
		}
		
		return table;
	}
	
	public static void main(String[] args)
	{
		try
		{
			out.println((new RandomGeneratorParser()).parse(""));
		}
		catch(ContentNotParseable e)
		{
			
		}
	}
}
